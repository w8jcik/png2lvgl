# png2lvgl

This script is a wrapper for the `ffmpeg` command. It converts selected PNG image to a format accepted by the LittlevGL. Such format is a RGB565 binary with some dedicated header added at the beginning.

## Setup

* Install `ffmpeg`.
* `$ ln -s png2lvgl ~/.local/bin/png2lvgl`

## Use

* `$ png2lvgl input.png output.bin`
