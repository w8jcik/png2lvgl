default:
	@echo ""
	@echo "  make install -- links png2lvgl to ~/.local/bin"
	@echo ""

install:
	if [ ! -e ~/.local/bin/png2lvgl ]; then       \
		ln -s png2lvgl ~/.local/bin/png2lvgl ;\
	fi
