#!/usr/bin/env python
from struct import *
from PIL import Image as image
import ntpath
import sys
import getopt
import os.path

cd = "16"
transp = "0"
fn = ""

def conf_init(argv):
    global fn
    global cd
    global transp

    try:
        opts, args = getopt.getopt(argv, "f:c:th", ["file=", "colordepth=", "chromakey=", "help"])
    except getopt.GetoptError:
        print 'Usage: python header.py -f <file name> [-c <color depth> -t <chroma key>]'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print 'Usage'
            print '   python img_conv.py -f <file name> [-c <color depth> -t <chroma key>]\n'
            print 'Options'
            print '  -f, --file         name of image to convert(e.g. my_pic.png)'
            print '  -c, --colordepth   color depth for the binary file, 8/16/24.                       Optional, default: 16 '
            print '  -t, --chromakey    mark as chrmakeyed (no value) needed                            Optional, default: off'
            print 'Example'
            print '  Convert an image with 24 bit color depth and no chroma key'
            print '    python img_conv.py -f file1.png -c 24\n'
            print '  Convert an image with 16 bit color depth and chrom akey'
            print '    python img_conv.py -f file2.png -c 16 -t'
            sys.exit()
        elif opt in ("-f", "--file"):
            fn = arg
        elif opt in ("-c", "--colordepth"):
            cd = arg
        elif opt in ("-t", "--chromakey"):
            transp = "1"

    if fn == "":
        print "ERROR: No image file specifeied"
        print 'img_conv.py -f <file name> [-c <color depth> -t <chroma key>]\n'
        exit()

    if os.path.exists(fn) is False:
        print "ERROR: " + fn + " not exists"
        exit()

    if cd != "8" and cd != "16" and cd != "24":
        print "ERROR: " + cd + " is an invalid color depth. Use 8, 16 or 24"
        exit()

def img_proc():
    global fn
    global cd
    global transp

    try:
        img = image.open(fn)
    except IOError as ioe:
        print "ERROR:", ioe
        print "ERROR: Try to convert the image to .jpg or .png format"
        exit()

    (w, h) = img.size
    data = list(img.getdata())

    cd_bin = 0
    if(cd == "8"):
        cd_bin = 1
    if(cd == "16"):
        cd_bin = 2
    if(cd == "24"):
        cd_bin = 3

    transp_bin = 0
    if(transp == "1"):
        transp_bin = 1

    header = (w & 0xFFF) | ((h & 0xFFF) << 12) | ((transp_bin & 0x1) << 24) | ((cd_bin & 0x3) << 25)

    sys.stdout.write(pack('<I', header))

def main(argv):
    conf_init(argv)
    img_proc()

if __name__ == "__main__":
    main(sys.argv[1:])

def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)
